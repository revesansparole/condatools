========================
condatools
========================

.. {# pkglts, doc

.. image:: https://revesansparole.gitlab.io/condatools/_images/badge_pkging_pip.svg
    :alt: PyPI version
    :target: https://pypi.org/project/condatools/0.0.1/

.. image:: https://revesansparole.gitlab.io/condatools/_images/badge_pkging_conda.svg
    :alt: Conda version
    :target: https://anaconda.org/revesansparole/condatools

.. image:: https://revesansparole.gitlab.io/condatools/_images/badge_doc.svg
    :alt: Documentation status
    :target: https://revesansparole.gitlab.io/condatools/

.. image:: https://badge.fury.io/py/condatools.svg
    :alt: PyPI version
    :target: https://badge.fury.io/py/condatools

.. #}
.. {# pkglts, glabpkg_dev, after doc

main: |main_build|_ |main_coverage|_

.. |main_build| image:: https://gitlab.com/revesansparole/condatools/badges/main/pipeline.svg
.. _main_build: https://gitlab.com/revesansparole/condatools/commits/main

.. |main_coverage| image:: https://gitlab.com/revesansparole/condatools/badges/main/coverage.svg
.. _main_coverage: https://gitlab.com/revesansparole/condatools/commits/main
.. #}

Extension tools for conda


@REM after installing this package in conda root environment
@REM need to be copied in ~\Miniconda3\Library\bin

@echo off

if [%1]==[] (
    for /f "delims=" %%i in ('pycharm_conda_env') do set cenv=%%i
) else (
    start cmd /k conda activate %1
    goto end
)
call conda activate %cenv%

if exist report\ (
  @REM launch companion programs
  start "" "%PROGRAMFILES%\Git\git-bash.exe"
  start pycharm64.exe .

  @REM compile and launch html
  cd report
  call make.bat clean
  call make html
  cd ..
  start firefox report/build/html/index.html

  @REM open shell in dirs
  start top_left /k "cd report & title doc"
  start full_right /k "cd script & title script"
  
  goto end
)

if exist script\ (
  @REM launch companion programs
  start "" "%PROGRAMFILES%\Git\git-bash.exe"
  start pycharm64.exe .

  @REM open shell in dirs
  start full_right /k "cd script & title script"

  goto end
)

if exist src\ (
  @REM launch companion programs
  start "" "%PROGRAMFILES%\Git\git-bash.exe"
  start pycharm64.exe .

  @REM open shell in dirs
  start full_right /k title script

  goto end
)

echo Default launch conda console
cmd /k

:end
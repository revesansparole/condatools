"""
Find name of environment as specified in pycharm
"""
from xml.etree.ElementTree import parse


def find_idea():
    for dir_pth in (".", "..", "../.."):
        try:
            return parse(f'{dir_pth}/.idea/misc.xml')
        except FileNotFoundError:
            pass

    raise FileNotFoundError("unable to locate idea/misc")


def main():
    tree = find_idea()
    root = tree.getroot()

    node, = [n for n in root if n.attrib.get('name', "") == "ProjectRootManager"]
    sdk = node.attrib['project-jdk-name']

    print(sdk)
